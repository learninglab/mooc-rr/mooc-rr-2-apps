# MOOC Recherche Reproductible

This repository contains everything to set up a jupyterhub environment.
It was intended the Reproducible Research course on https://fun-mooc.fr.

## Mirror Sunspot

A mirror from https://www.sidc.be/DATA/uset/Wlight/ has been done using wget on http://mooc-rr2.inria.fr/DATA/uset/Wlight/

To do so :

1. Connect with ssh to `mooc-rr2.inria.fr`
2. Go to `/data`
3. Then run wget using the following command

```sh
wget --mirror --convert-links --adjust-extension --no-parent --no-clobber --limit-rate=150m -nH --cut-dirs=2 -c -N --wait=0.2 https://www.sidc.be/DATA/uset/Wlight/
```

## Installation guide

### Set environment
This command generate the
```shell
make setenv env=[env]
```

### Optionnal (dev) : Self signed certificate

This command generate a self signed certificate to use for development purposes
```shell
openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ./secrets/certs/nginx-selfsigned.key -out ./secrets/certs/nginx-selfsigned.crt
```

This command generate Diffie-Helman parameters. This can take a long time:
```shell
openssl dhparam -out ./secrets/certs/dhparam.pem 4096
```

### Build Jupyter notebook image
This make script build the single user server image
```shell
make notebook_image
```

### Build Jupyterhub Docker image
This make script check and create all needed secrets files, docker volumes and docker networks
```shell
make build
```

### Run all services
```shell
docker-compose up -d
```

## Troubleshouting

- See all running/exited container related to jupyter `docker ps -a | grep jupyter`
- Access jupyterhub logs : `docker-compose logs hub`
- Access user logs : `docker logs jupyter-[user_id]`

## References :
- [Jupyter notebook](https://jupyter-notebook.readthedocs.io/en/stable/)
- [Jupyterhub](https://jupyterhub.readthedocs.io/en/stable/)
- [Jupyterhub Docker](https://github.com/jupyterhub/jupyterhub-deploy-docker)
- [Jupyterhub LTI Authenticator](https://github.com/jupyterhub/ltiauthenticator)
- [NbHosting de Thierry Parmentelat](https://github.com/parmentelat/nbhosting)
