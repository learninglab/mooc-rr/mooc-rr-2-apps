# Copyright (c) Jupyter Development Team.
# Distributed under the terms of the Modified BSD License.

# Configuration file for JupyterHub
import os
import sys
import hashlib
import requests
import urllib.parse
from dockerspawner import DockerSpawner

c = get_config()

# Spawn single-user servers as Docker containers
c.JupyterHub.spawner_class = DockerSpawner
# Spawn containers from this image
c.DockerSpawner.container_image = os.environ['DOCKER_NOTEBOOK_IMAGE']
# JupyterHub requires a single-user instance of the Notebook server, so we
# default to using the `start-singleuser.sh` script included in the
# jupyter/docker-stacks *-notebook images as the Docker run command when
# spawning containers.  Optionally, you can override the Docker run command
# using the DOCKER_SPAWN_CMD environment variable.
spawn_cmd = os.environ.get('DOCKER_SPAWN_CMD', "start-singleuser.sh")
c.DockerSpawner.extra_create_kwargs.update({ 'command': spawn_cmd })
# Connect containers to this Docker network
network_name = os.environ['DOCKER_NETWORK_NAME']
c.DockerSpawner.use_internal_ip = True
c.DockerSpawner.network_name = network_name
# Pass the network name as argument to spawned containers
c.DockerSpawner.extra_host_config = { 'network_mode': network_name, 'volume_driver': 'local' }
# Explicitly set notebook directory because we'll be mounting a host volume to
# it.  Most jupyter/docker-stacks *-notebook images run the Notebook server as
# user `jovyan`, and set the notebook directory to `/home/jovyan`.
# We follow the same convention.
notebook_dir ='/home/jovyan'
c.DockerSpawner.notebook_dir = notebook_dir
# Mount the real user's Docker volume on the host to the notebook user's
# notebook directory in the container
c.DockerSpawner.volumes = {'moocrr2_jupyterhub_user_{username}': notebook_dir}

# Singleuser container prefix (new and deprecated)
c.DockerSpawner.container_prefix = 'moocrr2-user'
c.DockerSpawner.prefix = 'moocrr2-user'

# Remove containers once they are stopped
c.DockerSpawner.remove_containers = True
# For debugging arguments passed to spawned containers
c.DockerSpawner.debug = True
# Set a memory limit
c.DockerSpawner.mem_limit = '2000M'



# User containers will access hub by container name on the Docker network
c.JupyterHub.hub_ip = 'moocrr2_jupyterhub'
c.JupyterHub.hub_port = 8080
c.JupyterHub.base_url = os.environ.get('MOOC_RR_HUB_PATH')

# Whitlelist admins
c.Authenticator.enable_auth_state = True
c.Authenticator.admin_users = admin = set()
c.JupyterHub.admin_access = True
pwd = os.path.dirname(__file__)
with open(os.path.join(pwd, 'userlist')) as f:
    for line in f:
        if not line:
            continue
        parts = line.split()
        name = parts[0]
        if len(parts) > 1 and parts[1] == 'admin':
            admin.add(name)

# Authenticate users with LTI
c.JupyterHub.authenticator_class = 'ltiauthenticator.lti11.auth.LTI11Authenticator'
c.LTI11Authenticator.consumers = {
    os.environ['LTI_KEY']:os.environ['LTI_SECRET']
}

# Persist hub data on volume mounted inside container
data_dir = os.environ.get('DATA_VOLUME_CONTAINER', '/data')

c.JupyterHub.cookie_secret_file = os.path.join(data_dir,
    'jupyterhub_cookie_secret')

c.JupyterHub.db_url = 'postgresql://postgres:{password}@{host}/{db}'.format(
    host=os.environ['POSTGRES_HOST'],
    password=os.environ['POSTGRES_PASSWORD'],
    db=os.environ['POSTGRES_DB'],
)

c.JupyterHub.services = [
    {
        "name": "idle-culler",
        "command": [
            sys.executable, "-m",
            "jupyterhub_idle_culler",
            "--timeout=600"
        ],
    }
]

c.JupyterHub.load_roles = [
    {
        "name": "idle-culler",
        "description": "Culls idle servers",
        "scopes": ["list:users","read:users:name", "read:users:activity", "delete:servers"],
        "services": ["idle-culler"],
    }
]
